# If fisher doesn't exist, download it
if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

# Daemon started in ~/.config/i3/config
set -gx SSH_AUTH_SOCK $XDG_RUNTIME_DIR/keyring/ssh

set -e -g __done_exclude
set -e -g EDITOR

bind \b 'backward-kill-word'  # Control-Backspace

kitty + complete setup fish | source
source ~/.asdf/asdf.fish

alias yadm="$HOME/.yadm-project/yadm"
# To set the user correctly

eval (starship init fish)

thefuck --alias | source
