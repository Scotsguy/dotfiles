# Defined in - @ line 1
function clipvid --description 'alias clipvid=xsel -b | youtube-dl -f 18 -a -'
    set -l link (xsel -b)
    echo $link
	youtube-dl --add-metadata --embed-thumbnail -f 18 $link $argv;
end
