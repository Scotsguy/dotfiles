# Defined in - @ line 1
function mkpatch --description 'alias mkpatch=git format-patch HEAD^ --stdout | haste --raw | xsel -b'
	git format-patch HEAD^ --stdout | haste --raw | xsel -b $argv;
end
