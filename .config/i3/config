set $mod Mod4

for_window [class="^.*"] border pixel 0
gaps inner 15 
gaps outer 15

bindsym $mod+z		gaps outer current plus 5
bindsym $mod+Shift+z	gaps outer current minus 5
# bindsym Mod4+Right 	workspace next
# bindsym Mod4+Left 	workspace prev

font pango:Jetbrains Mono

floating_modifier $mod

smart_gaps on
smart_borders on

# start a terminal
bindsym $mod+Return exec --no-startup-id kitty

# kill focused window
bindsym $mod+Shift+q kill

# start rofi
bindsym $mod+d exec --no-startup-id "rofi -modi drun,run -show drun"

# you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split v

# split in vertical orientation
bindsym $mod+v split h

# switched so that the mnemonic refers to the split line

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# Make the currently focused window a scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the first scratchpad window
bindsym $mod+minus scratchpad show

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1  General"
set $ws2 "2  Terminal"
set $ws3 "3  Chat"
set $ws4 "4  Code"
set $ws5 "5  Music"
set $ws6 "6  Games"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"
# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Color settings

set $bg-color 	         #2f343f
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935


#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          #424242
client.unfocused        $inactive-bg-color $inactive-bg-color $inactive-text-color #424242
client.focused_inactive $inactive-bg-color $inactive-bg-color $inactive-text-color #424242
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          #424242

bar {
   status_command i3blocks -c ~/.config/i3/i3blocks.conf
   position top
   height 21
   separator_symbol "〉"
   font pango:Jetbrains Mono 10
	colors {
	  separator #EA0AD3
    background #27012C
    	statusline #839496
    	focused_workspace #fdf6e3 #27012C #fdf6e3
    	active_workspace #fdf6e3  #fdf6e3
    	inactive_workspace #FB75ED #FB75ED #002b36
    	urgent_workspace #d33682 #d33682 #fdf6e3
   }
}

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +2% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -2% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle # mute sound

# Media player controls
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioStop exec --no-startup-id playerctl stop
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous

# Make Steam and its billion different windows behave

no_focus [class="Steam"]

assign [class="Steam"] $ws7

assign [class="discord"] $ws3

for_window [instance="origin.exe"] floating enable
assign [instance="origin.exe"] $ws7

for_window [class="Spotify"] move --no-back-and-forth to workspace $ws5

#            EDIT THIS SO IT FIT YOUR NEEDS
# ======================================================
# Set resoultion
exec --no-startup-id xrandr -s 1920x1080
# Set Background
exec --no-startup-id feh --bg-scale ~/Pictures/aesthetic.png
# Start notification service
exec --no-startup-id dunst
# Gnome Authentication Daemon
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id gnome-keyring-daemon --start
# PCManFM
exec --no-startup-id pcmanfm -d

# Tap Caps Lock: Escape
# Hold Caps Lock: Control
# (Latter set via locatectl)
exec --no-startup-id xcape -e 'Caps_Lock=Escape'

bindsym $mod+l exec --no-startup-id ~/scripts/corrupter-lock.sh

bindsym $mod+p exec --no-startup-id ~/scripts/powermenu-rofi.sh

# bindsym Print exec gnome-screenshot -i
# bindsym Ctrl+Print exec gnome-screenshot -i -a
bindsym Print exec --no-startup-id flameshot gui

bindsym $mod+c exec "i3-msg 'floating toggle; sticky toggle; resize shrink width 10000px; resize grow width 426px; resize shrink height 10000px; resize grow height 240px;move position 10px 10px;'"
#======================================================

