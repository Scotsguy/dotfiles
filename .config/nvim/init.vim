call plug#begin()
Plug 'dracula/vim', { 'as': 'dracula' }

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'tpope/vim-sensible'

Plug 'ycm-core/YouCompleteMe', { 'do': './install.py' }
Plug 'tpope/vim-surround'

Plug 'tpope/vim-repeat'

Plug 'neomake/neomake'
call plug#end()

color dracula
set termguicolors

set mouse=a

set shiftwidth=4
set autochdir

set smartcase
set ignorecase

" Kitty does not support background colour erase. Vim uses it anyway.
" This disables that.
" See Kitty's faq
let &t_ut=''
" Airline Customization
let g:airline_theme='dracula'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
