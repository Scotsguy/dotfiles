#!/bin/bash

# Options
power_off="⏻ Power off"
reboot="🔄 Reboot"
lock="🔒 Lock"
suspend="⏾ Suspend"
log_out="Log out"
cancel="Cancel"

options="$power_off\n$reboot\n$lock\n$suspend\n$log_out\n$cancel"

chosen="$(echo -e "$options" | rofi -dmenu -selected-row 2)"
case $chosen in
    $power_off)
        systemctl poweroff
        ;;
    $reboot)
        systemctl reboot
        ;;
    $lock)
        ~/scripts/corrupter-lock.sh
        ;;
    $suspend)
	playerctl pause
        systemctl suspend
        ;;
    $log_out)
        i3-msg exit
        ;;
    $cancel)
        ;;
esac
